import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the divisibleSumPairs function below.
    static int divisibleSumPairs(int n, int k, int[] ar) {
        int pairs=0;

        for(int i=0;i<ar.length;i++) {
            for (int j = 0; j < ar.length; j++) {
                if (i < j && (ar[i] + ar[j]) % k == 0) {
                    pairs++;
                }
            }
        }
        return pairs;
    }


    public static void main(String[] args) {


        int n = 6;

        int k = 3;

        int[] ar = {1,3,2,6,1,2};



        int result = divisibleSumPairs(n, k, ar);
        System.out.println(result);


    }
}
